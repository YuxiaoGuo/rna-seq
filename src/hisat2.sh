#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load hisat2
G=S_lycopersicum_Sep_2019 
O=hisat2
if [ ! -d $O ]; then
    mkdir $O
fi


# To run using qsub-doIt.sh:
#
#   qsub-doIt.sh _R1_001.fastq.gz hisat2.sh >jobs.out 2>jobs.err


# Assumes dUTP library prep protocol
#
# 5'---------------------->>>3' RNA
#    /2---->   <-----/1
# 
# read 1 is reverse complement of RNA
# read 2 is same sequence as RNA
#
# from: https://chipster.csc.fi/manual/library-type-summary.html

R2=$(echo "$F" | sed "s/R1/R2/")

### How do I tell it how to name the other files it produces?
hisat2  -p 8 --downstream-transcriptome-assembly --min-intronlen 20 --max-intronlen 13000 --rna-strandness RF -x $G -1 $F -2 $R2 -S $O/$S.sam
